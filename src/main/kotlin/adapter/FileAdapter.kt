package adapter

import model.EventModel
import serialize.serialize
import java.io.RandomAccessFile

class FileAdapter {
    private val path = "events.txt"
    private val eventsSource = RandomAccessFile(path, "rw")

    fun segmentPoll(start: Long, end: Long, segmentName: String = ""): EventModel? {
        eventsSource.seek(start)
        val builder = StringBuilder()
        var readedBytes = 0L
        var isPolled = false

        while (readedBytes < end && readedBytes < eventsSource.length()) {
            var bytes = eventsSource.read()
            readedBytes++

            if (bytes.toChar() == 'E') {
                // Mark event as read
                eventsSource.seek(start + readedBytes - 1)
                bytes = 'M'.toInt()
                eventsSource.writeByte(bytes)
                eventsSource.writeByte(' '.toInt())
                isPolled = true
            }

            // End of line
            if (bytes == '\n'.toInt()) {
                if (isPolled) {
                    return serialize(builder.toString())
                }

                builder.clear()
            } else {
                val parsedByte = bytes
                    .toChar()
                    .toString()
                    .replace("\n", "")

                 builder.append(parsedByte)

                 if (bytes == 'M'.toInt() && isPolled) {
                    builder.append(" ")
                 }
            }
        }

        return null
    }
}
