package serialize

import model.EventModel

fun serialize(input: String): EventModel {
    val splicedContent = input.split(" ")
    return EventModel(
        id = splicedContent[0].toLong(),
        content = splicedContent[2]
    )
}