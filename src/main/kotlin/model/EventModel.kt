package model

data class EventModel(
    val id: Long,
    val content: String
)